<?php

require 'vendor/autoload.php';

use App\Router;
use App\Controllers\Login;
use App\Controllers\Flatsharings;
use App\Controllers\Asked;
use App\Controllers\PersonalAsked;
use App\Controllers\Expenses;
use App\Controllers\PersonalExpenses;
use App\Controllers\Roommate;
use App\Controllers\Roommates;
use App\Controllers\Profil;
use App\Controllers\Priorities;
use App\Controllers\Tasks;

new Router([
  'login/:mail/:password' => Login::class,
  'flatsharings' => Flatsharings::class,
  'asked/:id' => Asked::class,
  'personalAsked/:fId/:id' => PersonalAsked::class,
  'expenses/:id' => Expenses::class,
  'personalExpenses/:fId/:id' => PersonalExpenses::class,
  'roommates/:id' => Roommates::class,
  'roommate/:id' => Roommate::class,
  'priorities' => Priorities::class,
  'tasks' => Tasks::class
]);
