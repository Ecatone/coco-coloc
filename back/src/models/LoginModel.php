<?php

namespace App\Models;

use \PDO;
use stdClass;

class LoginModel extends SqlConnect {
    public function get($mail, $password) {
      $req = $this->db->prepare("SELECT * FROM roommates WHERE mail = :mail AND password = :password");
      $req->execute(["mail" => $mail, "password" => $password]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }
}
