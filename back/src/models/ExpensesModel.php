<?php

namespace App\Models;

use \PDO;
use stdClass;

class ExpensesModel extends SqlConnect {
    public function getAll(int $id) {
      $req = $this->db->prepare("SELECT expenses.*, roommates.name AS roommateName
      FROM expenses 
      INNER JOIN roommates ON expenses.id_roommate = roommates.id
      where expenses.id_flatsharing = $id;");
      $req->execute();
  
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
    }
}
