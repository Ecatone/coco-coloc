<?php

namespace App\Models;

use \PDO;
use stdClass;

class PersonalExpensesModel extends SqlConnect {
    public function getAll(int $fId, int $id) {
      $req = $this->db->prepare("SELECT expenses.*, roommates.name AS roommateName
      FROM expenses 
      INNER JOIN roommates ON expenses.id_roommate = roommates.id
      where expenses.id_flatsharing = $fId AND expenses.id_roommate = $id;");
      $req->execute();
  
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
    }
}
