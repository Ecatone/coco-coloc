<?php

namespace App\Models;

use \PDO;
use stdClass;

class PrioritiesModel extends SqlConnect {
    public function getAll() {
      $req = $this->db->prepare("SELECT * FROM priorities");
      $req->execute();
  
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
    }
}
