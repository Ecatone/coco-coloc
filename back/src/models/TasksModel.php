<?php

namespace App\Models;

use \PDO;
use stdClass;

class TasksModel extends SqlConnect {
    public function getAll() {
      $req = $this->db->prepare("SELECT tasks.id, tasks.name, tasks_categories.name 
      AS CategoryName, tasks_categories.color AS CategoryColor FROM tasks 
      INNER JOIN tasks_categories ON tasks.id_tasks_category = tasks_categories.id;");
      $req->execute();
  
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
    }
}
