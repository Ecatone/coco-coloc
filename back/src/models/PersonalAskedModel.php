<?php

namespace App\Models;

use \PDO;
use stdClass;

class PersonalAskedModel extends SqlConnect {
    public function getAll(int $fid, int $id) {
      $req = $this->db->prepare("SELECT asked_tasks.id, asked_tasks.description,
      asked_tasks.due_date, asked_tasks.is_finished, tasks_categories.color,
      flatsharings.name AS flatsharingName, tasks.name AS taskName,
      tasks.logo AS taskLogo, roommates.name AS roommateName, 
      priorities.name AS priorityName
      FROM asked_tasks
      INNER JOIN flatsharings ON asked_tasks.id_flatsharing = flatsharings.id
      INNER JOIN tasks ON asked_tasks.id_task = tasks.id
      INNER JOIN tasks_categories ON tasks.id_tasks_category = tasks_categories.id
      INNER JOIN roommates ON asked_tasks.id_roommate = roommates.id
      INNER JOIN priorities ON asked_tasks.id_priority = priorities.id
      WHERE asked_tasks.id_flatsharing = $fid AND asked_tasks.id_roommate = $id;");
      $req->execute();
  
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
    }
}
