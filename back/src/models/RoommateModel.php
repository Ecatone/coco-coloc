<?php

namespace App\Models;

use \PDO;
use stdClass;

class RoommateModel extends SqlConnect {
    public function add(array $data) {
      $query = "
        INSERT INTO roommates (name, mail, password, color)
        VALUES (:name, :mail, :password, :color)
      ";

      $req = $this->db->prepare($query);
      $req->execute($data);
    }

    public function delete(int $id) {
      $req = $this->db->prepare("DELETE FROM roommates WHERE id = :id");
      $req->execute(["id" => $id]);
    }

    public function get(int $id) {
      $req = $this->db->prepare("SELECT roommates.*, flatsharings.code AS flatsharingCode 
      FROM roommates INNER JOIN flatsharings ON roommates.id_flatsharing = flatsharings.id 
      WHERE roommates.id = :id;");
      $req->execute(["id" => $id]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getLast() {
      $req = $this->db->prepare("SELECT * FROM roommates ORDER BY id DESC LIMIT 1");
      $req->execute();

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }
}
