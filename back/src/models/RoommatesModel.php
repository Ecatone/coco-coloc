<?php

namespace App\Models;

use \PDO;
use stdClass;

class RoommatesModel extends SqlConnect {
    public function getAll(int $id) {
      $req = $this->db->prepare("SELECT * FROM roommates where roommates.id_flatsharing = $id");
      $req->execute();
  
      return $req->rowCount() > 0 ? $req->fetchAll(PDO::FETCH_ASSOC) : new stdClass();
    }
}
