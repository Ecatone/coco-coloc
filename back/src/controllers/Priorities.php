<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\PrioritiesModel;

class Priorities extends Controller {
  protected object $priorities;

  public function __construct($param) {
    $this->priorities = new PrioritiesModel();

    parent::__construct($param);
  }

  public function getPriorities() {
    return $this->priorities->getAll();
  }
}
