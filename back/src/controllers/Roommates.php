<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\RoommatesModel;

class Roommates extends Controller {
  protected object $roommates;

  public function __construct($param) {
    $this->roommates = new RoommatesModel();

    parent::__construct($param);
  }

  public function getRoommates() {
    return $this->roommates->getAll(intval($this->params['id']));
  }
}
