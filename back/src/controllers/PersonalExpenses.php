<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\PersonalExpensesModel;

class PersonalExpenses extends Controller {
  protected object $personalExpenses;

  public function __construct($param) {
    $this->personalExpenses = new PersonalExpensesModel();
    parent::__construct($param);
  }

  public function getPersonalExpenses() {
    return $this->personalExpenses->getAll(intval($this->params['fId']), intval($this->params['id']));
  }
}
