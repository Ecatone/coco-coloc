<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\RoommateModel;

class Roommate extends Controller {
  protected object $roommate;

  public function __construct($param) {
    $this->roomate = new RoommateModel();

    parent::__construct($param);
  }

  public function postRoommate() {
    $this->roomate->add($this->body);

    return $this->roomate->getLast();
  }

  public function deleteRoommate() {
    return $this->roomate->delete(intval($this->params['id']));
  }

  public function getRoommate() {
    return $this->roomate->get(intval($this->params['id']));
  }
}
