<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\LoginModel;

class Login extends Controller {
  protected object $login;

  public function __construct($param) {
    $this->login = new LoginModel();

    parent::__construct($param);
  }

  public function getLogin() {
    return $this->login->get($this->params['mail'],$this->params['password']);
  }
}
