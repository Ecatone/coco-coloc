<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\AskedModel;

class Asked extends Controller {
  protected object $asked;

  public function __construct($param) {
    $this->asked = new AskedModel();
    parent::__construct($param);
  }

  public function getAsked() {
    return $this->asked->getAll(intval($this->params['id']));
  }
}
