<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\FlatsharingsModel;

class Flatsharings extends Controller {
  protected object $flatsharings;

  public function __construct($param) {
    $this->flatsharings = new FlatsharingsModel();

    parent::__construct($param);
  }

  public function getFlatsharings() {
    return $this->flatsharings->getAll();
  }
}
