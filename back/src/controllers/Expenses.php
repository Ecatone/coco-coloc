<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\ExpensesModel;

class Expenses extends Controller {
  protected object $expenses;

  public function __construct($param) {
    $this->expenses = new ExpensesModel();

    parent::__construct($param);
  }

  public function getExpenses() {
    return $this->expenses->getAll(intval($this->params['id']));
  }
}
