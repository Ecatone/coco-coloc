<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\PersonalAskedModel;

class PersonalAsked extends Controller {
  protected object $personalAsked;

  public function __construct($param) {
    $this->personalAsked = new PersonalAskedModel();
    parent::__construct($param);
  }

  public function getPersonalAsked() {
    return $this->personalAsked->getAll(intval($this->params['fId']), intval($this->params['id']));
  }
}
