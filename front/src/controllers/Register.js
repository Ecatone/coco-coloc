import axios from 'axios';

import viewHeader from '../views/header_logged_out';
import viewRegister from '../views/register';

const Register = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.run();
  }

  render() {
    return `
    ${viewHeader()}
    ${viewRegister()}
    `;
  }

  async run() {
    this.el.innerHTML = this.render(this.datas);
    document.querySelector('#login').addEventListener('submit', (e) => {
      e.preventDefault();
      const name = document.getElementsByName('name')[0];
      const mail = document.getElementsByName('mail')[0];
      const password = document.getElementsByName('password')[0];
      const confirmPassword = document.getElementsByName('password')[0];
      const RegisterError = document.querySelector('.register-error');

      if (name.value === '' || mail.value === '' || password.value === '' || confirmPassword.value === '') {
        RegisterError.innerHTML = 'Un des champs est vide';
      }
      if (password.value === confirmPassword.value) {
        axios.post('http://localhost:82/login/', {
          name: name.value,
          mail: mail.value,
          password: password.value
        })
          .then((res) => {
            if (mail.value === res.data.mail && password.value === res.data.password) {
              window.location = 'http://127.0.0.1:9090/calendar';
            }
          });
      }
    });
  }
};

export default Register;
