import axios from 'axios';

import viewHeader from '../views/header_logged_in';
import viewExpenses from '../views/expenses';

const Expenses = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.datas = {};
    this.run();
  }

  render(datas) {
    return `
    ${viewHeader(datas.user)}
    ${viewExpenses(datas.expenses)}
    `;
  }

  getCookie(name) {
    const matches = document.cookie.match(new RegExp(
      `(?:^|; )${name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1')}=([^;]*)`
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  eraseCookie(name) {
    document.cookie = `${name}=; Max-Age=-99999999;`;
  }

  async run() {
    this.datas.user = {
      id: this.getCookie('userId'),
      id_flatsharing: this.getCookie('userId_flatsharing'),
      name: this.getCookie('userName'),
      color: this.getCookie('userColor')
    };
    await axios.get(`http://localhost:82/expenses/${this.datas.user.id_flatsharing}`).then((res) => { this.datas.expenses = res.data; });
    this.el.innerHTML = this.render(this.datas);
    const logOut = document.querySelector('.log-out');
    logOut.addEventListener('click', () => {
      this.eraseCookie('userName');
      this.eraseCookie('userId');
      this.eraseCookie('userId_flatsharing');
      this.eraseCookie('userColor');
      window.location = 'http://127.0.0.1:9090/';
    });
  }
};

export default Expenses;
