import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import axios from 'axios';

import viewHeader from '../views/header_logged_in';
import viewCalendar from '../views/get_calendar';

const GetCalendar = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.datas = {};

    this.run();
  }

  render(datas) {
    return `
    ${viewHeader(datas.user)}
    ${viewCalendar(datas)}
    `;
  }

  getCookie(name) {
    const matches = document.cookie.match(new RegExp(
      `(?:^|; )${name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1')}=([^;]*)`
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  eraseCookie(name) {
    document.cookie = `${name}=; Max-Age=-99999999;`;
  }

  async run() {
    this.datas.user = {
      id: this.getCookie('userId'),
      id_flatsharing: this.getCookie('userId_flatsharing'),
      name: this.getCookie('userName'),
      color: this.getCookie('userColor')
    };
    // 2024-06-15
    await axios.get('http://localhost:82/roommates/1').then((res) => { this.datas.roommates = res.data; });
    await axios.get('http://localhost:82/tasks').then((res) => { this.datas.tasks = res.data; });
    await axios.get('http://localhost:82/priorities').then((res) => { this.datas.priorities = res.data; });
    this.el.innerHTML = this.render(this.datas);
    const calendarEl = document.getElementById('calendar');
    const calendar = new Calendar(calendarEl, {
      plugins: [dayGridPlugin, interactionPlugin],
      initialView: 'dayGridMonth',
      editable: true, // important for activating event interactions!
      selectable: true, // important for activating date selectability!
      dateClick: () => {
        // const elInfo = info;
        // alert(`Date: ${info.dateStr}`);
        // elInfo.dayEl.style.backgroundColor = 'red';
      }
    });
    calendar.render();
    const logOut = document.querySelector('.log-out');
    logOut.addEventListener('click', () => {
      this.eraseCookie('userName');
      this.eraseCookie('userId');
      this.eraseCookie('userId_flatsharing');
      this.eraseCookie('userColor');
      window.location = 'http://127.0.0.1:9090/';
    });
  }
};

export default GetCalendar;
