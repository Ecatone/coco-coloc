import axios from 'axios';

import viewHeader from '../views/header_logged_out';
import viewLogin from '../views/login';

const Login = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.run();
  }

  render() {
    return `
    ${viewHeader()}
    ${viewLogin()}
    `;
  }

  async run() {
    this.el.innerHTML = this.render(this.datas);
    document.querySelector('#login').addEventListener('submit', (e) => {
      e.preventDefault();
      const mail = document.getElementsByName('mail')[0];
      const password = document.getElementsByName('password')[0];
      const loginError = document.querySelector('.login-error');

      if (mail.value === '' || password.value === '') {
        loginError.innerHTML = 'Un des champs est vide';
      } else {
        axios.get(`http://localhost:82/login/${mail.value}/${password.value}`)
          .then((res) => {
            if (mail.value === res.data.mail && password.value === res.data.password) {
              document.cookie = `userName=${res.data.name}; path=/;`;
              document.cookie = `userId=${res.data.id}; path=/;`;
              document.cookie = `userId_flatsharing=${res.data.id_flatsharing}; path=/;`;
              document.cookie = `userColor=${res.data.color}; path=/;`;
              if (res.data.id_flatsharing === 0) {
                window.location = 'http://127.0.0.1:9090/get-flatsharing';
              } else {
                window.location = 'http://127.0.0.1:9090/coco-coloc';
              }
            }
          });
      }
    });
  }
};

export default Login;
