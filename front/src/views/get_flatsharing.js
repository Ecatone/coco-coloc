export default () => (`
<main class="d-flex">
    <div class="m-auto p-5 text-center border border-black rounded-3">
      <h3 class="mb-5">Pas encore de coloc ?</h3>
      <div class="d-flex">
        <form class="card me-4" method="get" style="width: 18rem;">
          <img class="card-img-top" src="http://localhost:83/Coco_coloc.png" alt="Card image cap">
          <hr>
          <div class="card-body">
            <h5 class=" m-3">Créer colocation</h5>
            <label class="ms-3">Name :
              <input class="p-1 rounded-2" type="text" placeholder="ex : Coco Coloc'">
            </label>
            <div class="d-flex"><button type="submit" class="mx-auto my-3 p-1 btn btn-secondary">confirm</button></div>
          </div>
        </form>
        <form class="card" style="width: 18rem;">
          <img class="card-img-top" src="http://localhost:83/Coco_coloc.png" alt="Card image cap">
          <hr>
          <div class="card-body">
            <h5 class=" m-3">Rejoindre colocation</h5>
            <label class="ms-3">Code :
              <input class="p-1 rounded-2" type="text" placeholder="ex : Ex5g0pG3d23h">
            </label>
            <div class="d-flex">
              <button type="submit" class="mx-auto my-3 p-1 btn btn-secondary">confirm</button>
            </div>
          </div>
        </form>
      </div>
    </div>
</main>
`);
