export default (expense) => (`
  <div class="mx-5 mb-2 card">
    <div class="ps-3 py-1 card-header">
      <p>Assignée à : ${expense.roommateName}</p>
    </div>
    <div class="p-3 card-body">
      <h5 class="mb-2 card-title">${expense.name}</h5>
      <p class="card-text">${expense.amount}€</p>
      <p class="card-text text-end">Payé : ${(expense.is_payed === 1 ? 'OUI' : 'NON')}</p>
    </div>
  </div>
`);
