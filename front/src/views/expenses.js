import viewExpense from './expense';

export default (expenses) => (`
<main>
    <div class="pb-3">
        ${expenses.map((expense) => viewExpense(expense)).join('')}
    </div>
</main>
`);
