export default (priority) => (`
  <option value="${priority.id}">${priority.name}</option>
`);
