import viewAsked from './asked';
import viewExpense from './expense';

export default (asked, expenses) => (`
<main class="pb-5 d-flex">
  <div class="w-50">
    ${asked.map((ask) => viewAsked(ask)).join('')}
  </div>
  <div class="w-50">
    ${expenses.map((expense) => viewExpense(expense)).join('')}
  </div>
</main>
`);
