export default () => (`
<main class="d-flex text-center">
  <form method="get" id="login" class="m-auto p-5 border border-black rounded-3">
    <p>Connecter-vous :</p>
    <div class="py-3 d-flex flex-column gap-3 align-items-end">
      <p class="login-error"></p>
      <label>Mail :
        <input class="p-1 rounded-2" type="text" name="mail" placeholder="ex : exemple@gmail.com">
      </label>
      <label>Password :
        <input class="p-1 rounded-2" type="text" name="password">
      </label>
    </div>
    <p>Pas encore incsrit ? <br> Inscrivez-vous <a href="#">ici</a></p>
    <button class="mt-2 p-2 btn btn-secondary" type="submit" value="submit">connection</button>
  </form>
</main>
`);
