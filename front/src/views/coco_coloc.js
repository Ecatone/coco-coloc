import viewAsked from './asked';

export default (asked) => (`
<main>
  <div class="pb-3">
    ${asked.map((ask) => viewAsked(ask)).join('')}
  </div>
</main>
`);
