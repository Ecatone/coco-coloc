export default (roommate) => (`
  <option value="${roommate.id}">${roommate.name}</option>
`);
