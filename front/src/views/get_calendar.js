import viewTask from './task';
import viewRoommate from './roommate';
import viewPriority from './priority';

export default (datas) => (`
<main class="d-flex text-center">
  <div id="calendar" class="m-3 w-75"></div>
  <form method="get" class="my-3 mx-2 border border-black rounded-3">
    <div class="d-flex justify-content-between">
      <h3 class="ms-3 p-3">XX jour du XX mois :</h3>
      <button class="my-3 me-5 py-1 px-3 btn btn-secondary" type="submit">Modifier</button>
    </div>
    <hr>
    <div class="p-5 d-flex flex-column gap-3">
      <select class="p-2 form-select form-select-lg" aria-label=".form-select-lg example">
        <option selected>Sélectionner une tâche</option>
        ${datas.tasks.map((task) => viewTask(task)).join('')}
      </select>
      <select class="p-2 form-select form-select-lg" aria-label=".form-select-lg example">
        <option selected>Sélectionner un colocataire</option>
        ${datas.roommates.map((roommate) => viewRoommate(roommate)).join('')}
      </select>
      <select class="mb-3 p-2 form-select form-select-lg" aria-label=".form-select-lg example">
        <option selected>Définisser une priorité</option>
        ${datas.priorities.map((priority) => viewPriority(priority)).join('')}
      </select>
    </div>
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
      <label class="form-check-label" for="flexCheckDefault">
        Default checkbox
      </label>
    </div>
  </form>
</main>
`);
