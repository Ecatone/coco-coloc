export default (user) => (`
<header class="py-2">
  <div class="mx-2 d-flex justify-content-between">
    <div><img src="http://localhost:83/Coco_coloc.png" alt="Logo du site Coco coloc'" width="150"></div>
    <div class="d-flex align-items-center">
      <p>Welcome ${user.name}</p>
      <div class="ms-2" id="cercle" style="background: ${user.color};"></div>
      <button class="mx-3 log-out btn btn-white">Déconnection</button>
    </div>
  </div>
  <hr>
  <nav class="py-3 mx-5 d-flex gap-5">
    <a href="http://127.0.0.1:9090/coco-coloc">Coco coloc</a>
    <a href="http://127.0.0.1:9090/expenses">Dépenses</a>
    <a href="http://127.0.0.1:9090/personal-space">Suivi perso</a>
    <a href="http://127.0.0.1:9090/profil">Profil</a>
    <a href="http://127.0.0.1:9090/calendar">Calendar</a>
  </nav>
  <hr>
</header>
`);
