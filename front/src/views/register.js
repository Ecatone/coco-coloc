export default () => (`
<main class="d-flex text-center">
  <form method="post" class="m-auto p-5 border border-black rounded-3">
    <p>Inscription :</p>
    <div class="py-3 d-flex flex-column gap-3 align-items-end">
      <label for="">Username :
        <input class="p-1 rounded-2" type="text" name="name" placeholder="ex : User1234">
      </label>
      <label for="">Mail :
        <input class="p-1 rounded-2" type="text" name="mail" placeholder="ex : exemple@gmail.com">
      </label>
      <label for="">Password :
        <input class="p-1 rounded-2" type="text" name="password">
      </label>
      <label for="">Confirm Password :
        <input class="p-1 rounded-2" type="text" name="confirmPassword">
      </label>
    </div>
    <button class="mt-2 p-2 btn btn-secondary" type="submit">inscription</button>
  </form>
</main>
`);
