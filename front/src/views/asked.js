export default (ask) => (`
  <div class="mx-5 mb-3 card">
    <div class="ps-3 py-2 card-header text-light" style = "background:${ask.color}">
      <p><b>Pour le : ${ask.due_date}</b></p>
      <p>assignée à : ${ask.roommateName}</p>
    </div>
    <div class="p-3 card-body">
      <h5 class="mb-2 card-title">${ask.taskName}</h5>
      <p class="card-text">${ask.description}</p>
      <p class="card-text text-end">Terminer : ${(ask.is_finished === 1 ? 'OUI' : 'NON')}</p>
    </div>
  </div>
`);
