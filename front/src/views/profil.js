export default (user) => (`
<main class="d-flex text-center">
  <form method="get" class="m-auto p-5 border border-black rounded-3">
    <h3>Profil :</h3>
    <div class="py-3 d-flex flex-column gap-3 align-items-end">
      <label for="">Username :
        <input class="p-1 rounded-2" type="text" value="${user.name}">
      </label>
      <label for="">Mail :
        <input class="p-1 rounded-2" type="text" value="${user.mail}">
      </label>
      <label for="">Password :
        <input class="p-1 rounded-2" type="text">
      </label>
      <label for="">New Password :
        <input class="p-1 rounded-2" type="text">
      </label>
      <label for="">Confirm New Password :
        <input class="p-1 rounded-2" type="text">
      </label>
    </div>
    <p>colocation code : ${user.flatsharingCode}</p>
    <button class="mt-3 p-2 btn btn-secondary" type="submit">update</button>
  </form>
</main>
`);
