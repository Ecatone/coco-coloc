import Router from './Routeur';
import Login from './controllers/Login';
import Register from './controllers/Register';
import GetFlatsharing from './controllers/GetFlatsharing';
import GetCalendar from './controllers/GetCalendar';
import CocoColoc from './controllers/CocoColoc';
import Expenses from './controllers/Expenses';
import PersonalSpace from './controllers/PersonalSpace';
import Profil from './controllers/Profil';

import './index.scss';

const routes = [{
  url: '/',
  controller: Login
}, {
  url: '/register',
  controller: Register
}, {
  url: '/get-flatsharing',
  controller: GetFlatsharing
}, {
  url: '/calendar',
  controller: GetCalendar
}, {
  url: '/coco-coloc',
  controller: CocoColoc
}, {
  url: '/expenses',
  controller: Expenses
}, {
  url: '/personal-space',
  controller: PersonalSpace
}, {
  url: '/profil',
  controller: Profil
}];

new Router(routes);
