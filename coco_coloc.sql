-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : dim. 16 juin 2024 à 17:31
-- Version du serveur : 8.0.37-0ubuntu0.22.04.3
-- Version de PHP : 8.1.2-1ubuntu2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `coco_coloc`
--
CREATE DATABASE IF NOT EXISTS `coco_coloc` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `coco_coloc`;

-- --------------------------------------------------------

--
-- Structure de la table `asked_tasks`
--

CREATE TABLE `asked_tasks` (
  `id` int NOT NULL,
  `id_flatsharing` int NOT NULL,
  `id_task` int NOT NULL,
  `id_roommate` int NOT NULL,
  `id_priority` int NOT NULL,
  `due_date` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_finished` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `asked_tasks`
--

INSERT INTO `asked_tasks` (`id`, `id_flatsharing`, `id_task`, `id_roommate`, `id_priority`, `due_date`, `description`, `is_finished`) VALUES
(3, 1, 1, 2, 2, '2025-06-15', 'Sortir les poubelles.', 0),
(4, 1, 6, 3, 4, '2025-06-17', 'Il faut aller acheter des rouleaux de papier toilette et du savon pour le lavabo de la douche', 0);

-- --------------------------------------------------------

--
-- Structure de la table `expenses`
--

CREATE TABLE `expenses` (
  `id` int NOT NULL,
  `id_flatsharing` int NOT NULL,
  `id_roommate` int NOT NULL,
  `name` varchar(60) NOT NULL,
  `amount` decimal(6,2) NOT NULL,
  `is_payed` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `expenses`
--

INSERT INTO `expenses` (`id`, `id_flatsharing`, `id_roommate`, `name`, `amount`, `is_payed`) VALUES
(1, 1, 2, '3 pizza large Dominos', '24.00', 0),
(2, 1, 2, '3 pizza large Dominos', '23.97', 0),
(3, 1, 3, '3 pizza large Dominos', '23.97', 0),
(4, 1, 2, '3 pizza large Dominos', '23.96', 0),
(5, 1, 3, '3 pizza large Dominos', '23.97', 0);

-- --------------------------------------------------------

--
-- Structure de la table `flatsharings`
--

CREATE TABLE `flatsharings` (
  `id` int NOT NULL,
  `name` varchar(60) NOT NULL,
  `code` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `flatsharings`
--

INSERT INTO `flatsharings` (`id`, `name`, `code`) VALUES
(1, 'LaColoc', '123456');

-- --------------------------------------------------------

--
-- Structure de la table `priorities`
--

CREATE TABLE `priorities` (
  `id` int NOT NULL,
  `name` varchar(60) NOT NULL,
  `color` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `priorities`
--

INSERT INTO `priorities` (`id`, `name`, `color`) VALUES
(1, 'Pas importante', '#D3D3D3'),
(2, 'Peu importante', '#0000FF'),
(3, 'Importante', '#ED7F10'),
(4, 'Très importante', '#FF0000');

-- --------------------------------------------------------

--
-- Structure de la table `roommates`
--

CREATE TABLE `roommates` (
  `id` int NOT NULL,
  `id_flatsharing` int NOT NULL,
  `name` varchar(60) NOT NULL,
  `mail` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `color` varchar(10) NOT NULL,
  `is_creator` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `roommates`
--

INSERT INTO `roommates` (`id`, `id_flatsharing`, `name`, `mail`, `password`, `color`, `is_creator`) VALUES
(1, 0, 'Guilaume', 'test@test.test', '12345', '#D3D3D3', 0),
(2, 1, 'Brigite', 'test2@test2.test2', '1234', '#D5B60A', 1),
(3, 1, 'Robert', 'test3@test3.test3', '1234', '#D5D5D5', 0);

-- --------------------------------------------------------

--
-- Structure de la table `tasks`
--

CREATE TABLE `tasks` (
  `id` int NOT NULL,
  `id_tasks_category` int NOT NULL,
  `name` varchar(60) NOT NULL,
  `logo` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `tasks`
--

INSERT INTO `tasks` (`id`, `id_tasks_category`, `name`, `logo`) VALUES
(1, 1, 'Poubelle', ''),
(2, 1, 'Carton', ''),
(3, 1, 'Verre', ''),
(4, 2, 'Nourriture', ''),
(5, 2, 'Produits lessive/lave vaisselle', ''),
(6, 2, 'Savon/Papier toilette', ''),
(7, 3, 'Serpillière', ''),
(8, 3, 'Aspirateur', ''),
(9, 3, 'Poussière', ''),
(10, 4, 'Cuisine', ''),
(11, 4, 'Rangement frigo', ''),
(12, 4, 'Lave vaisselle', '');

-- --------------------------------------------------------

--
-- Structure de la table `tasks_categories`
--

CREATE TABLE `tasks_categories` (
  `id` int NOT NULL,
  `name` varchar(60) NOT NULL,
  `color` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `tasks_categories`
--

INSERT INTO `tasks_categories` (`id`, `name`, `color`) VALUES
(1, 'Déchets', '#00561b'),
(2, 'Courses', '#ED7F10'),
(3, 'Ménage', '#0000FF'),
(4, 'Cuisine', '#FF0000');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `asked_tasks`
--
ALTER TABLE `asked_tasks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `flatsharings`
--
ALTER TABLE `flatsharings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roommates`
--
ALTER TABLE `roommates`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tasks_categories`
--
ALTER TABLE `tasks_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `asked_tasks`
--
ALTER TABLE `asked_tasks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `flatsharings`
--
ALTER TABLE `flatsharings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `roommates`
--
ALTER TABLE `roommates`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `tasks_categories`
--
ALTER TABLE `tasks_categories`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
